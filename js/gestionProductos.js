var productosObtenidos;

function procesarProductos(){
  var JSONProductos = JSON.parse(productosObtenidos);
  //alert(JSONProductos.value[0].ProductName);
  var divTabla=document.getElementById("divTabla");
  var tabla=document.createElement("table");
  var tbody=document.createElement("tbody");
  
  tabla.classList.add("table");
  tabla.classList.add("table-striped");
  
  for (var i=0; i<JSONProductos.value.length; i++){
    var nuevaFila=document.createElement("tr");
    var columnaNombre=document.createElement("td");
    var columnaPrecio=document.createElement("td");
    var columnaStock=document.createElement("td");
    
    columnaNombre.innerText = JSONProductos.value[i].ProductName;
    columnaPrecio.innerText = JSONProductos.value[i].UnitPrice;
    columnaStock.innerText = JSONProductos.value[i].UnitsInStock;

    nuevaFila.append(columnaNombre);
    nuevaFila.append(columnaPrecio);
    nuevaFila.append(columnaStock);
    
    tbody.appendChild(nuevaFila);
  }
  
  tabla.appendChild(tbody);
  divTabla.append(tabla);
}

function getProductos(){
  var url="https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request=new XMLHttpRequest();
  request.onreadystatechange=function(){
    if (this.readyState == 4 && this.status == 200){
      //console.log(request.responseText);
      productosObtenidos = request.responseText;
      procesarProductos();
    }
  }
  request.open("GET", url, true);
  request.send();
}